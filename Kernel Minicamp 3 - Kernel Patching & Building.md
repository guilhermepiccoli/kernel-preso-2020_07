# Kernel Minicamp 3
Patching, Building and testing



## Step 1
Getting the correct kernel sources


#### Start with the correct series...
- xenial:\
  [git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/xenial][xenial-git]
- bionic:\
  [git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/bionic][bionic-git]
- focal:\
  [git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/focal][focal-git]

[xenial-git]: https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/xenial
[bionic-git]: https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/bionic
[focal-git]: https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/focal


#### ...or special flavor
- linux-aws:\
  [git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-aws][linux-aws]
- linux-azure:\
  [git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-azure][linux-azure]
- linux-gcp:\
  [git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-gcp][linux-gcp]

e.g. for xenial-aws:\
[git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-aws/+git/xenial][xenial-aws]

[linux-aws]: https://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-aws
[linux-azure]: https://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-azure
[linux-gcp]: https://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-gcp
[xenial-aws]: https://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-aws/+git/xenial


#### "How am I supposed to know all that?"
[`kernel-series.yaml`][series]

```yaml
# 18.04 (bionic)
'18.04':
  codename: bionic
  development: false
  supported: true
  lts: true
...
  sources:
    linux:
      versions: ['4.13.0', '4.14.0', '4.15.0']
      packages:
        linux:
          repo: ['git://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/bionic', 'master-next']
...
    linux-aws:
      packages:
        linux-aws:
          repo: ['git://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-aws/+git/bionic']
      derived-from: [ '18.04', 'linux' ]
```

[series]: https://git.launchpad.net/~canonical-kernel/+git/kteam-tools/tree/info/kernel-series.yaml


#### Be mindful of hwe kernels
```bash
ubuntu@bionic:~$ uname -rv
4.15.0-111-generic #112-Ubuntu SMP Thu Jul 9 20:32:34 UTC 2020
```

```bash
ubuntu@xenial-hwe:~$ uname -rv
4.15.0-107-generic #108~16.04.1-Ubuntu SMP Fri Jun 12 02:57:13 UTC 2020
```

- linux-hwe "borrows" the kernel from next release

Note: Version.Major.Minor-Patch #Build


#### How to compare different kernel versions
[`kernel-version-map`][kernel-versions]

| Ubuntu Kernel Version | Ubuntu Kernel Tag | Mainline Kernel Version |
| --------------------- | ----------------- | ----------------------- |
| 5.4.0-9.12  | Ubuntu-5.4.0-9.12  | 5.4.3  |
| 5.4.0-18.22 | Ubuntu-5.4.0-18.22 | 5.4.24 |
| 5.4.0-21.25 | Ubuntu-5.4.0-21.25 | 5.4.27 |
| 5.4.0-24.28 | Ubuntu-5.4.0-24.28 | 5.4.30 |

[kernel-versions]: https://people.canonical.com/~kernel/info/kernel-version-map.html


#### `git` instead of `pull-lp-source`
- easier to backport/cherry-pick commits from upstream
- re-use kernel repo for multiple cases (tip: `git help worktree`)
- hotfix-helper needs to work on a git repo
- SRU process "suits" git (`git send-email`)



## The kernel source tree
Upstream:
```bash
arch/    Documentation/  init/    LICENSES/  scripts/   usr/     Kbuild       README
block/   drivers/        ipc/     mm/        security/  virt/    Kconfig
certs/   fs/             kernel/  net/       sound/     COPYING  MAINTAINERS
crypto/  include/        lib/     samples/   tools/     CREDITS  Makefile
```

Ubuntu (Focal):
```bash
arch/    debian.master/  init/      mm/        sound/   COPYING      MAINTAINERS
block/   Documentation/  ipc/       net/       tools/   CREDITS      Makefile
certs/   drivers/        kernel/    samples/   ubuntu/  dropped.txt  README
crypto/  fs/             lib/       scripts/   usr/     Kbuild       snapcraft.yaml
debian/  include/        LICENSES/  security/  virt/    Kconfig      update-version-dkms*
```

Note: On older Ubuntu kernels, we might still have the zfs/spl folders


#### Neat in-tree stuff
- Documentation! (also on [kernel.org][kernel-docs])
```bash
$ find Documentation/ -type f | wc -l
6886
```
- scripts! \
  (`decodecode`, `checkpatch.pl`, `tags.sh`, ...)
- explore!

[kernel-docs]: https://www.kernel.org/doc/html/latest/


## Patching


#### Anatomy of our commits
```text
  commit e1c03b95657fe607ef61577fb3383f17eb7cf4c6
  Author: Johannes Berg <johannes.berg@intel.com>
  Date:   Wed Mar 4 14:08:18 2020 +0800

      iwlwifi: remove IWL_DEVICE_22560/IWL_DEVICE_FAMILY_22560

      BugLink: https://bugs.launchpad.net/bugs/1865962

      This is dead code, nothing uses the IWL_DEVICE_22560 macro and
      thus nothing every uses IWL_DEVICE_FAMILY_22560. Remove it all.

      While at it, remove some code and definitions used only in this
      case, and clean up some comments/names that still refer to it.

      Signed-off-by: Johannes Berg <johannes.berg@intel.com>
      Signed-off-by: Luca Coelho <luciano.coelho@intel.com>
      Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
      (backported from commit 3681021fc6af58bfacd1c6e7b1e03ea1da7681e2)
      Signed-off-by: AceLan Kao <acelan.kao@canonical.com>
      Signed-off-by: Seth Forshee <seth.forshee@canonical.com>
```


#### Using upstream commits
- git log --grep <upstream_hash>
- git apply 0000-my-beautiful-upstream.patch
- git cherry-pick <upstream_hash> \
  (tip: add `torvalds/linux` as a remote, or use git clone --reference)


#### Sometimes we need to do some more work
- New patches might need refactoring before applying to old series \
  ("backported from" vs "cherry picked from")
- Our tree can diverge from upstream due to e.g. backports
- Patches can depend on things that we don't have in our trees
- Test and validate changes before sending out packages :)

Note: Just because it compiles does not mean it's correct!


#### And sometimes we use the force
```bash
$ git bisect start
$ git bisect old Ubuntu-5.4.0-8.11
$ git bisect new Ubuntu-5.4-5.4.0-14.17
```
- Binary search for specific commit that introduced a "change"
- Can take a lot of time (especially because of rebuild/testing)
- Best suited for known-regressions or between "close" kernel versions



## Testing & Building
- "manual" build: [`fakeroot debian/rules`][fakeroot]
- "customer" build: `hotfix-helper` from [sup-eng-tools repo][sup-eng-tools]

[fakeroot]: https://wiki.ubuntu.com/Kernel/BuildYourOwnKernel
[sup-eng-tools]: https://code.launchpad.net/~canonical-support-eng/sup-eng-tools/+git/sup-eng-tools


#### Recommended build environment
- same toolchain as target (e.g. bionic on bionic and so on)
- beefy container or host (CPU)
- export DEB_BUILD_OPTS="parallel=56"
- export DEBFULLNAME="Your Name Here"
- export DEBEMAIL="me@canonical.com"
- CCache



## SRU
- By email, to [`kernel-team@lists.ubuntu.com`][kernel-list] \
  (tip: `git format-patch`, `git send-email`)
- Needs approval from at least two kernel-team members
- Kernel cycles, keep expectations in check ([kernel.ubuntu.com][kernel-cycle])
- More info: [KernelTeam/KernelUpdates][kernel-updates]

[kernel-list]: https://lists.ubuntu.com/archives/kernel-team/
[kernel-cycle]: https://kernel.ubuntu.com
[kernel-updates]: https://wiki.ubuntu.com/KernelTeam/KernelUpdates
