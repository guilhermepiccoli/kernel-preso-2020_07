# Overview of Kernel Traces

An introduction to kernel traces (as seen in `dmesg`, `/var/log/kern.log` or the system `console`);
what kind of information is available, how to read it, where (not) to focus, and some tools.

The goal is to allow non-kernel folks to become more familiar with kernel traces, what to look for, and how to use it.

# Part 1 - General Structure of a Kernel Trace
---

The kernel traces can be split in different sections.  

The timestamps are *seconds.microseconds* since system boot (uptime).

- Header with a summary of the error, and a few details:
```
[ 16.460954] BUG: unable to handle kernel NULL pointer dereference at 0000000000000008
[ 16.463150] IP: create_empty_buffers+0x29/0xf0
[ 16.464548] PGD 800000012aa90067 P4D 800000012aa90067 PUD 12aa91067 PMD 0
[ 16.466582] Oops: 0000 [#1] SMP PTI
```
- What kernel modules are loaded:
```
[ 16.467702] Modules linked in: bcache isofs kvm_intel kvm irqbypass joydev input_leds serio_raw ib_iser rdma_cm iw_cm ib_cm ib_core iscsi_tcp libiscsi_tcp libiscsi scsi_transport_iscsi autofs4 btrfs xor zstd_compress raid6_pq psmouse virtio_net virtio_blk floppy
```
- What process hit the error, on which CPU, the kernel version, any kernel taint flags:
```
[ 16.472651] CPU: 0 PID: 1467 Comm: bcache-register Not tainted 4.15.0-110-generic #111-Ubuntu
```
- On which platform (e.g., qemu, another hypervisor/emulator, cloud, bare metal):
```
[ 16.474238] Hardware name: QEMU Standard PC (i440FX + PIIX, 1996), BIOS 1.10.2-1ubuntu1 04/01/2014
```
- CPU registers (kernel space):
```
[ 16.475756] RIP: 0010:create_empty_buffers+0x29/0xf0
[ 16.476715] RSP: 0018:ffffa9a740abb838 EFLAGS: 00010246
[ 16.477745] RAX: 0000000000000000 RBX: ffffebd404aa77c0 RCX: 000000000000000d
[ 16.479113] RDX: 0000000000000000 RSI: 0000000000002000 RDI: ffffebd404aa77c0
[ 16.480422] RBP: ffffa9a740abb850 R08: 0000000000026d60 R09: 0000000000000687
[ 16.481742] R10: 0000000000000002 R11: ffff91e6bffd2000 R12: 0000000000000000
[ 16.482976] R13: 0000000000000000 R14: 0000000000000000 R15: 0000000000000200
[ 16.484304] FS: 00007f2c87adb700(0000) GS:ffff91e6bfc00000(0000) knlGS:0000000000000000
[ 16.485929] CS: 0010 DS: 0000 ES: 0000 CR0: 0000000080050033
[ 16.487084] CR2: 0000000000000008 CR3: 000000012a994000 CR4: 00000000000006f0
```
- Stack trace of functions, instruction offset/total, and kernel modules:
```
[ 16.488488] Call Trace:
[ 16.489042] create_page_buffers+0x51/0x60
[ 16.489924] block_read_full_page+0x4e/0x3a0
[ 16.490820] ? set_init_blocksize+0x80/0x80
[ 16.491680] ? pagevec_lru_move_fn+0xc3/0xe0
[ 16.492560] ? __lru_cache_add+0x58/0x70
[ 16.493418] blkdev_readpage+0x18/0x20
[ 16.494219] do_read_cache_page+0x2a3/0x580
[ 16.495090] ? blkdev_writepages+0x40/0x40
[ 16.495937] ? __switch_to_asm+0x35/0x70
[ 16.496768] ? __switch_to_asm+0x41/0x70
[ 16.497611] ? __switch_to_asm+0x35/0x70
[ 16.498455] ? __switch_to_asm+0x41/0x70
[ 16.499297] read_cache_page+0x15/0x20
[ 16.500086] read_dev_sector+0x2d/0xe0
[ 16.500858] read_lba+0x130/0x220
[ 16.501603] ? kmem_cache_alloc_trace+0x15d/0x1d0
[ 16.502557] efi_partition+0x138/0x790
[ 16.503344] ? string+0x60/0x90
[ 16.504003] ? vsnprintf+0xfb/0x510
[ 16.504729] ? snprintf+0x45/0x70
[ 16.505436] ? is_gpt_valid.part.6+0x420/0x420
[ 16.506310] check_partition+0x130/0x230
[ 16.507116] ? is_gpt_valid.part.6+0x420/0x420
[ 16.508045] ? check_partition+0x130/0x230
[ 16.508891] rescan_partitions+0xaa/0x350
[ 16.509728] bdev_disk_changed+0x53/0x60
[ 16.510524] __blkdev_get+0x34a/0x510
[ 16.511283] blkdev_get+0x129/0x320
[ 16.511998] ? wake_up_bit+0x42/0x50
[ 16.512736] ? unlock_new_inode+0x4c/0x80
[ 16.513593] ? bdget+0x108/0x120
[ 16.514280] device_add_disk+0x38b/0x490
[ 16.515106] ? vprintk_default+0x29/0x50
[ 16.515939] bch_cached_dev_run.part.28+0x42/0x1a0 [bcache]
[ 16.517033] register_bcache+0x8ab/0x1110 [bcache]
[ 16.518012] ? __handle_mm_fault+0xf43/0x1290
[ 16.518869] kobj_attr_store+0x12/0x20
[ 16.519661] ? kobj_attr_store+0x12/0x20
[ 16.520449] sysfs_kf_write+0x3c/0x50
[ 16.521204] kernfs_fop_write+0x125/0x1a0
[ 16.522036] __vfs_write+0x1b/0x40
[ 16.522754] vfs_write+0xb1/0x1a0
[ 16.523458] SyS_write+0x5c/0xe0
[ 16.524176] do_syscall_64+0x73/0x130
[ 16.524933] entry_SYSCALL_64_after_hwframe+0x41/0xa6
```
- CPU registers (user space):
```
[ 16.525919] RIP: 0033:0x7f2c875eb2c0
[ 16.526636] RSP: 002b:00007ffe51b11878 EFLAGS: 00000246 ORIG_RAX: 0000000000000001
[ 16.528107] RAX: ffffffffffffffda RBX: 000000000000000b RCX: 00007f2c875eb2c0
[ 16.529490] RDX: 000000000000000b RSI: 0000000000cdd010 RDI: 0000000000000003
[ 16.530829] RBP: 0000000000cdd010 R08: 0000000000000000 R09: 000000000000000b
[ 16.532156] R10: 000000000000000a R11: 0000000000000246 R12: 000000000000000b
[ 16.533487] R13: 0000000000000001 R14: 00007ffe51b118f0 R15: 0000000000000000
```
- CPU instructions around the error/faulting instruction:
```
[ 16.534841] Code: 00 00 0f 1f 44 00 00 55 48 89 e5 41 55 41 54 53 49 89 d5 ba 01 00 00 00 48 89 fb e8 22 ff ff ff 49 89 c4 48 89 c2 eb 03 48 89 ca <48> 8b 4a 08 4c 09 2a 48 85 c9 75 f1 4c 89 62 08 48 8b 43 08 48
 ```
 - CPU register summary:
 ```
[ 16.538311] RIP: create_empty_buffers+0x29/0xf0 RSP: ffffa9a740abb838
[ 16.539545] CR2: 0000000000000008
```
- Footer:
```
[ 16.540247] ---[ end trace 88ea84610b23a19a ]---
```

# Part 2 - Details of each Section
---

## Header

There are different types of header messages / kernel errors, which reflect different types of problems.  
The messages might vary slightly across different kernel versions.

Some examples:

### BUG()
```
[3016161.866702] kernel BUG at /build/linux-3busU_/linux-4.4.0/include/linux/fs.h:2583!
[3016161.866704] invalid opcode: 0000 [#1] SMP 
```

The `BUG()` call indicates a problem has been detected; it's a check for incorrect/invalid values and conditions.  
For example, `if (x < 0) BUG();` or, similarly, `BUG_ON(x < 0);`.

The `invalid opcode` message is not the _cause_ of the issue, it's an _effect_ of the `BUG()` implementation.  
On `x86` the implementation of `BUG()` inserts an `ud2` instruction (*undefined instruction*) to trigger an error handler.

In this case:
```
static inline void i_readcount_dec(struct inode *inode)
{
        BUG_ON(!atomic_read(&inode->i_readcount));
        atomic_dec(&inode->i_readcount);
}
```

### WARN()
```
[388820.395663] NETDEV WATCHDOG: eth4 (igb): transmit queue 2 timed out
[388820.395696] WARNING: CPU: 1 PID: 0 at /build/linux-hwe-Lz9SGa/linux-hwe-4.15.0/net/sched/sch_generic.c:323 dev_watchdog+0x222/0x230
```

The `WARN()` / `WARN_ON()` / `WARN_ONCE()` calls are similar to `BUG()`, but usually for less serious conditions or issues, which the kernel should be able to handle and recover.

In this case:
```
    if (some_queue_timedout) {
            WARN_ONCE(1, KERN_INFO "NETDEV WATCHDOG: %s (%s): transmit queue %u timed out\n",
                   dev->name, netdev_drivername(dev), i);
            dev->netdev_ops->ndo_tx_timeout(dev);
    }
```

### Bad pointer / invalid memory address (a.k.a. *Oops*)

- Close to zero/NULL (`< PAGE_SIZE`):
```
[ 16.460954] BUG: unable to handle kernel NULL pointer dereference at 0000000000000008
[ 16.463150] IP: create_empty_buffers+0x29/0xf0
[ 16.464548] PGD 800000012aa90067 P4D 800000012aa90067 PUD 12aa91067 PMD 0
[ 16.466582] Oops: 0000 [#1] SMP PTI
```

- Far from zero/NULL (`>= PAGE_SIZE`):
```
[ 4549.861046] BUG: unable to handle kernel paging request at ffffffff8ca8f400
[ 4549.866137] IP: __pv_queued_spin_lock_slowpath+0x1a7/0x290
[ 4549.871281] PGD 1ef420e067 P4D 1ef420e067 PUD 1ef420f063 PMD 1e74d07063 PTE 80003fe10bb70062
[ 4549.879281] Oops: 0002 [#2] SMP PTI
```

The different messages just indicate whether the memory address is "low or high":
- "low" addresses tend to be memory accesses to `struct` members/fields on top of a `NULL` pointer.
- "high" addresses tend to be memory access to invalid pointer (no longer valid, use after free) or incorrect pointer arithmetic.

```
show_fault_oops(...)
{
    ...
    printk(KERN_ALERT "BUG: unable to handle kernel ");
    if (address < PAGE_SIZE)
            printk(KERN_CONT "NULL pointer dereference");
    else
            printk(KERN_CONT "paging request");
    ...
}
```

- General protection fault (GPF)
```
Oct  3 09:17:07 <...> kernel: general protection fault: 0000 [#1] SMP PTI
```

This usually indicates a memory access to an address in a way that violates the protection bits configured for its page.  
For example:
- write to read-only page,
- execute instructions from non-executable page,
- memory address in non-canonical form   
  (canonical form addresses: first 16 bits are either `0xffff` or `0x0000` -- bits 48-63 are copies of bit 47)

The `Oops` code (four digits in all 3 types above) provide more details of what exactly went wrong.
See [`arch/x86/include/asm/traps.h`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/arch/x86/include/asm/traps.h)
```
/*
 * Page fault error code bits:
 *
 *   bit 0 ==    0: no page found       1: protection fault
 *   bit 1 ==    0: read access         1: write access
 *   bit 2 ==    0: kernel-mode access  1: user-mode access
 *   bit 3 ==                           1: use of reserved bit detected
 *   bit 4 ==                           1: fault was an instruction fetch
 *   bit 5 ==                           1: protection keys block access
 */
 ```
 
To the right of the `Oops` code there are features/flags the kernel has enabled:
- `SMP` (Symmetric Multi Processing), support for multiple CPUs
- `PTI` (Page Table Isolation), mitigation for the meltdown CPU vulnerability is enabled (`NOPTI` when disabled)
 
The `PGD/P4D/PUD/PMD/PTE` values are page-table specific addresses, usually not interesting/an issue on most cases.
 
### Hung task
```
[  364.050416] INFO: task kworker/u49:4:395 blocked for more than 120 seconds.
```

The task/process has been waiting for something (e.g., locks as mutex/semaphore) held by other task/process, or that it failed to release properly.  
The stack trace should reveal what is that.

### Soft lockup
```
Oct  3 09:17:34 <...> kernel: watchdog: BUG: soft lockup - CPU#7 stuck for 23s! [mumps:369336]
```

The `soft lockup` message indicates a task/process that keeps running without voluntarily rescheduling, hogging the CPU it's scheduled on.  
The CPU can still handle/service interrupts, e.g., timer, so the scheduler *can* still run other tasks on it.

### Hard lockup
```
[ 393.628647] NMI watchdog: Watchdog detected hard LOCKUP on cpu 30
```

The `hard lockup` message indicates a task/process that keeps running with interrupts disabled/masked.
The CPU cannot handle/service interrupts, e.g., timer, so the scheduler *cannot* run other tasks on it.

This is detected via `NMI` (non-maskable interrupt), which the CPU cannot ignore/mask.

### Crash/Panic

Not every kernel error causes the kernel to crash/panic (although it might make the system unusable or prevent progress at the process- or system-level; e.g., fail while holding locks).
This depends on the respective `panic_on_*` / `*_panic` sysctl settings.

For example, only *panic on oops* is enabled below:

```
$ grep . /proc/sys/kernel/*panic*
/proc/sys/kernel/hardlockup_panic:0
/proc/sys/kernel/hung_task_panic:0
/proc/sys/kernel/panic:0
/proc/sys/kernel/panic_on_io_nmi:0
/proc/sys/kernel/panic_on_oops:1
/proc/sys/kernel/panic_on_rcu_stall:0
/proc/sys/kernel/panic_on_unrecovered_nmi:0
/proc/sys/kernel/panic_on_warn:0
/proc/sys/kernel/panic_print:0
/proc/sys/kernel/softlockup_panic:0
/proc/sys/kernel/unknown_nmi_panic:0
```

## Kernel modules loaded and taints

This lists the kernel modules currently loaded, and the ones with module *taint* flags if any.
```
[ 4489.780835] Modules linked in: algif_hash af_alg binfmt_misc sch_fq tcp_bbr kyber_iosched zfs(PO) zunicode(PO) zavl(PO) icp(PO) zcommon(PO) znvpair(PO) spl(O) serio_raw crct10dif_pclmul crc32_pclmul ghash_clmulni_intel pcbc aesni_intel aes_x86_64 crypto_simd glue_helper cryptd sch_fq_codel ip_tables x_tables autofs4 btrfs zstd_compress raid10 raid456 async_raid6_recov async_memcpy async_pq async_xor async_tx xor raid6_pq libcrc32c raid1 raid0 multipath linear ena(OE)
```

Modules with taint flags:
-  `zfs(PO) zunicode(PO) zavl(PO) icp(PO) zcommon(PO) znvpair(PO)` -- **P**roprietary and **O**ut-of-tree.
-  `spl(O)` -- **O**ut-of-tree
-  `ena(OE)` -- **O**ut-of-tree and unsign**E**d.

Note: the ZFS modules shipped in the Ubuntu kernels report `PO` and `O` (first couple of bullets above.)  
Other out-of-tree modules are not supported by us, and might cause issues (needs analysis/ask to repro w/out.)

See [Documentation/admin-guide/tainted-kernels.rst](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/admin-guide/tainted-kernels.rst) for details on other taint flags.
```
===  ===  ======  ========================================================
Bit  Log  Number  Reason that got the kernel tainted
===  ===  ======  ========================================================
  0  G/P       1  proprietary module was loaded
  1  _/F       2  module was force loaded
  2  _/S       4  SMP kernel oops on an officially SMP incapable processor
  3  _/R       8  module was force unloaded
  4  _/M      16  processor reported a Machine Check Exception (MCE)
  5  _/B      32  bad page referenced or some unexpected page flags
  6  _/U      64  taint requested by userspace application
  7  _/D     128  kernel died recently, i.e. there was an OOPS or BUG
  8  _/A     256  ACPI table overridden by user
  9  _/W     512  kernel issued warning
 10  _/C    1024  staging driver was loaded
 11  _/I    2048  workaround for bug in platform firmware applied
 12  _/O    4096  externally-built ("out-of-tree") module was loaded
 13  _/E    8192  unsigned module was loaded
 14  _/L   16384  soft lockup occurred
 15  _/K   32768  kernel has been live patched
 16  _/X   65536  auxiliary taint, defined for and used by distros
 17  _/T  131072  kernel was built with the struct randomization plugin
===  ===  ======  ========================================================
```


## CPU/PID/Task/Taints/Version

Who, where:

- `[ 16.472651] CPU: 0 PID: 1467 Comm: bcache-register Not tainted 4.15.0-110-generic #111-Ubuntu`
  - CPU: 0
  - PID: 1467
  - Comm: bcache-register
  - Not tainted
  - *4.15.0-110-generic #111-Ubuntu* (from `uname -rv`)

- `[ 4489.804152] CPU: 0 PID: 13414 Comm: cache-worker Tainted: P           OE    4.15.0-1065-aws #69-Ubuntu`
  - CPU: 0
  - PID: 13414
  - Comm: cache-worker
  - Tainted: P OE
  - *4.15.0-1065-aws #69-Ubuntu* (from `uname -rv`)

Note: check for HWE kernel (i.e., running in the previous LTS release) with the version suffixes `~14.04.x` / `~16.04.x` / `~18.04.x`  
For example, the 4.15 "GA" kernel from Bionic/18.04 as "HWE" kernel on Xenial/16.04:
- ```CPU: 1 PID: 0 Comm: swapper/1 Tainted: P           OE    4.15.0-70-generic #79~16.04.1-Ubuntu```

## Hardware/Platform/System type

This shows where the kernel is running on,  and may provide hints about old BIOS versions.

- `[ 16.474238] Hardware name: QEMU Standard PC (i440FX + PIIX, 1996), BIOS 1.10.2-1ubuntu1 04/01/2014`
- `[388820.395833] Hardware name: Dell Inc. PowerEdge R640/0RGP26, BIOS 2.3.10 08/15/2019`
- `[295928.260399] Hardware name: OpenStack Foundation OpenStack Nova, BIOS 1.10.2-1ubuntu1~cloud0 04/01/2014 `
- `[ 393.628705] Hardware name: NVIDIA NVIDIA DGX-2/NVIDIA DGX-2, BIOS 0.010 07/02/2018`
- `Oct  3 09:17:07 ... Hardware name: VMware, Inc. VMware Virtual Platform/440BX Desktop Reference Platform, BIOS 6.00 09/19/2018`

## CPU Registers

There may be one or two sets of CPU registers in the trace: from kernel space and user space, depending on whether user space is involved (e.g., system call.)  
Usually only the kernel space register set matters.

The register set and values are architecture dependent; `x86_64` a.k.a. `amd64` is the most common for our customers.

- `[ 16.475756] RIP: 0010:create_empty_buffers+0x29/0xf0`
The *Instruction Pointer Register*, points to which instruction is running when the issue happened.
It is printed as function name `+` instruction offset within the function (in bytes) `/` function size (total bytes for instructions.)
This can be used to identify the instruction in the function's disassembly or its specific line of code.
  
- `[ 16.476715] RSP: 0018:ffffa9a740abb838 EFLAGS: 00010246`
The *Stack Pointer Register* and the *Flags* register.
Both are not really interesting/useful for most issues; it indicates where your stack is and some CPU state (usually sane.)


- General purpose registers (most of them are GPRs)

These 15 registers hold the variables/pointers/constants used in the code.  

```
[ 16.477745] RAX: 0000000000000000 RBX: ffffebd404aa77c0 RCX: 000000000000000d
[ 16.479113] RDX: 0000000000000000 RSI: 0000000000002000 RDI: ffffebd404aa77c0
[ 16.480422] RBP: ffffa9a740abb850 R08: 0000000000026d60 R09: 0000000000000687
[ 16.481742] R10: 0000000000000002 R11: ffff91e6bffd2000 R12: 0000000000000000
[ 16.482976] R13: 0000000000000000 R14: 0000000000000000 R15: 0000000000000200
```

Some of them are "special" for function calls in the kernel ABI (Application Binary Interface):
- **RDI, RSI, RDX, RCX, R8, R9**: function parameters (up to 6 arguments in registers)
- **RAX**: function return value

The contents of CPU registers might help to understand more about the kernel error
with just kernel trace messages/no crashdump yet available; but not always sufficient.

Looking at the instructions and registers is not easy at first, but the kernel usually
only uses "simple" instructions (not advanced math or stuff), which are well documented
and commented on in google/stack overflow/etc, thus feasible to get it "right enough."

- Segment Registers and Control Registers

The *Segment Registers* were used for memory segmentation in the past; not anymore for paging/virtual memory.
**FS** and **GS** are still used for other, low-level things (per-thread/thread-local and per-cpu storage.)
**CRx** registers configure the CPU, and can tell CPU state.

Both types are not really interesting/useful for most issues.  
**Except** for **CR2**, which indicates the faulting (instruction/data) memory address, but that is usually present in the error message already.

```
[ 16.484304] FS: 00007f2c87adb700(0000) GS:ffff91e6bfc00000(0000) knlGS:0000000000000000
[ 16.485929] CS: 0010 DS: 0000 ES: 0000 CR0: 0000000080050033
[ 16.487084] CR2: 0000000000000008 CR3: 000000012a994000 CR4: 00000000000006f0
```

## Stack/Call Trace

This tells you the code path that hit the issue.  
- It is _most recent first_ / "bottom-up" (different from Python, which is _most recent last_ / "top-down") 
- It only prints what is actually in the stack/memory -- the **current** function is in `RIP` (_Instruction Pointer Register_)
- The question marks indicate stack frames/entries that are *unreliable* / could not be proven to be part of the stack -- just ignore those for starters.
- If a function is in a kernel module (a `module.ko` file, not the `vmlinux` file), it is listed as `[module]` (see `bcache` below.)

With question marks:
```
[ 16.475756] RIP: 0010:create_empty_buffers+0x29/0xf0
...
[ 16.488488] Call Trace:
[ 16.489042] create_page_buffers+0x51/0x60
[ 16.489924] block_read_full_page+0x4e/0x3a0
[ 16.490820] ? set_init_blocksize+0x80/0x80
[ 16.491680] ? pagevec_lru_move_fn+0xc3/0xe0
[ 16.492560] ? __lru_cache_add+0x58/0x70
[ 16.493418] blkdev_readpage+0x18/0x20
[ 16.494219] do_read_cache_page+0x2a3/0x580
[ 16.495090] ? blkdev_writepages+0x40/0x40
[ 16.495937] ? __switch_to_asm+0x35/0x70
[ 16.496768] ? __switch_to_asm+0x41/0x70
[ 16.497611] ? __switch_to_asm+0x35/0x70
[ 16.498455] ? __switch_to_asm+0x41/0x70
[ 16.499297] read_cache_page+0x15/0x20
[ 16.500086] read_dev_sector+0x2d/0xe0
[ 16.500858] read_lba+0x130/0x220
[ 16.501603] ? kmem_cache_alloc_trace+0x15d/0x1d0
[ 16.502557] efi_partition+0x138/0x790
[ 16.503344] ? string+0x60/0x90
[ 16.504003] ? vsnprintf+0xfb/0x510
[ 16.504729] ? snprintf+0x45/0x70
[ 16.505436] ? is_gpt_valid.part.6+0x420/0x420
[ 16.506310] check_partition+0x130/0x230
[ 16.507116] ? is_gpt_valid.part.6+0x420/0x420
[ 16.508045] ? check_partition+0x130/0x230
[ 16.508891] rescan_partitions+0xaa/0x350
[ 16.509728] bdev_disk_changed+0x53/0x60
[ 16.510524] __blkdev_get+0x34a/0x510
[ 16.511283] blkdev_get+0x129/0x320
[ 16.511998] ? wake_up_bit+0x42/0x50
[ 16.512736] ? unlock_new_inode+0x4c/0x80
[ 16.513593] ? bdget+0x108/0x120
[ 16.514280] device_add_disk+0x38b/0x490
[ 16.515106] ? vprintk_default+0x29/0x50
[ 16.515939] bch_cached_dev_run.part.28+0x42/0x1a0 [bcache]
[ 16.517033] register_bcache+0x8ab/0x1110 [bcache]
[ 16.518012] ? __handle_mm_fault+0xf43/0x1290
[ 16.518869] kobj_attr_store+0x12/0x20
[ 16.519661] ? kobj_attr_store+0x12/0x20
[ 16.520449] sysfs_kf_write+0x3c/0x50
[ 16.521204] kernfs_fop_write+0x125/0x1a0
[ 16.522036] __vfs_write+0x1b/0x40
[ 16.522754] vfs_write+0xb1/0x1a0
[ 16.523458] SyS_write+0x5c/0xe0
[ 16.524176] do_syscall_64+0x73/0x130
[ 16.524933] entry_SYSCALL_64_after_hwframe+0x41/0xa6
```

Without question marks (manually removed):
```
[ 16.475756] RIP: 0010:create_empty_buffers+0x29/0xf0
...
[ 16.488488] Call Trace:
[ 16.489042] create_page_buffers+0x51/0x60
[ 16.489924] block_read_full_page+0x4e/0x3a0
[ 16.493418] blkdev_readpage+0x18/0x20
[ 16.494219] do_read_cache_page+0x2a3/0x580
[ 16.499297] read_cache_page+0x15/0x20
[ 16.500086] read_dev_sector+0x2d/0xe0
[ 16.500858] read_lba+0x130/0x220
[ 16.502557] efi_partition+0x138/0x790
[ 16.506310] check_partition+0x130/0x230
[ 16.508891] rescan_partitions+0xaa/0x350
[ 16.509728] bdev_disk_changed+0x53/0x60
[ 16.510524] __blkdev_get+0x34a/0x510
[ 16.511283] blkdev_get+0x129/0x320
[ 16.514280] device_add_disk+0x38b/0x490
[ 16.515939] bch_cached_dev_run.part.28+0x42/0x1a0 [bcache]
[ 16.517033] register_bcache+0x8ab/0x1110 [bcache]
[ 16.518869] kobj_attr_store+0x12/0x20
[ 16.520449] sysfs_kf_write+0x3c/0x50
[ 16.521204] kernfs_fop_write+0x125/0x1a0
[ 16.522036] __vfs_write+0x1b/0x40
[ 16.522754] vfs_write+0xb1/0x1a0
[ 16.523458] SyS_write+0x5c/0xe0
[ 16.524176] do_syscall_64+0x73/0x130
[ 16.524933] entry_SYSCALL_64_after_hwframe+0x41/0xa6
```

## Real Example

SF#280403 -- Eric identified based on the kernel traces alone that an out-of-tree kernel module was loaded and involved in the stack trace (2 frames without question marks.) Case closed.

```
[2061202.458549] BUG: unable to handle kernel NULL pointer dereference at 0000000000000010
[2061202.466757] IP: __radix_tree_lookup+0xe/0xf0
[2061202.471311] PGD 0 P4D 0
[2061202.474135] Oops: 0000 [#1] SMP PTI
[2061202.477909] Modules linked in: ... CyProtectDrv(OE) ...
[2061202.535087] CPU: 6 PID: 8920 Comm: agent Tainted: G OE 4.15.0-1058-gcp #62-Ubuntu
[2061202.543975] Hardware name: Google Google Compute Engine/Google Compute Engine, BIOS Google 01/01/2011
[2061202.553474] RIP: 0010:__radix_tree_lookup+0xe/0xf0
...
[2061202.638495] Call Trace:
[2061202.641223] <IRQ>
[2061202.643540] ? unicast_user+0x130/0x130 [CyProtectDrv]
[2061202.648969] radix_tree_lookup+0xd/0x10
[2061202.653084] find_get_pid+0x3a/0x50
[2061202.656873] CY_Get_Task_From_Pid+0x44/0xe0 [CyProtectDrv]
[2061202.662640] ? unicast_user+0x130/0x130 [CyProtectDrv]
[2061202.668062] cy_ctrl_timer_fn+0x35/0x80 [CyProtectDrv]
[2061202.673479] call_timer_fn+0x32/0x140
[2061202.677423] ? unicast_user+0x130/0x130 [CyProtectDrv]
[2061202.682840] run_timer_softirq+0x1f4/0x450
[2061202.687230] ? kvm_clock_get_cycles+0x1e/0x20
[2061202.691867] ? ktime_get+0x3e/0xb0
[2061202.695547] ? native_apic_msr_write+0x2b/0x30
[2061202.700295] ? lapic_next_event+0x20/0x30
[2061202.704595] __do_softirq+0x126/0x2ec
[2061202.708536] irq_exit+0xda/0xe0
[2061202.711967] smp_apic_timer_interrupt+0x79/0x150
[2061202.716863] apic_timer_interrupt+0x96/0xa0
[2061202.721335] </IRQ> 
```

## Instructions nearby

The kernel reads and prints the instructions nearby the faulting instruction (the one within `<` `>` marks).  
This helps to confirm the instructions that are running (e.g., in case of memory corruption), and give you a partial disassembly without debug symbols.
There's a tool to convert it (`decodecode`, covered below).

```
[ 16.534841] Code: 00 00 0f 1f 44 00 00 55 48 89 e5 41 55 41 54 53 49 89 d5 ba 01 00 00 00 48 89 fb e8 22 ff ff ff 49 89 c4 48 89 c2 eb 03 48 89 ca <48> 8b 4a 08 4c 09 2a 48 85 c9 75 f1 4c 89 62 08 48 8b 43 08 48
```

# Part 3 - Tools
---

## Kernel debug symbols (pre-requisite)

Get the kernel debug symbols (*dbgsym*) package for the ***exact*** kernel version that is failing. (Watch out for HWE version suffix, e.g., `~16.04.x`.)

See the [Debug Symbol Packages](https://wiki.ubuntu.com/Debug%20Symbol%20Packages) wiki page for options, or use `pull-*-ddebs` from `ubuntu-dev-tools`.
You can also download directly from [ddebs.ubuntu.com/pool/main/l/](http://ddebs.ubuntu.com/pool/main/l/) in some `*linux*` source package directory:

`$ wget http://ddebs.ubuntu.com/pool/main/l/linux/linux-image-3.13.0-24-generic-dbgsym_3.13.0-24.47_amd64.ddeb # 401M`

(Note, on newer kernels, look for `linux-image-unsigned-<version>-dbgsym` even though running a `signed` kernel)

Hint: extract just the `vmlinux` kernel image and kernel modules needed (if any), to save disk space (huge size):

- Full extraction:
```
$ dpkg-deb -x linux-image-3.13.0-24-generic-dbgsym_3.13.0-24.47_amd64.ddeb ddeb

$ du -hs ddeb/usr/lib/debug/
2.8G    ddeb/usr/lib/debug/
```
- Partial extraction:
```
$ dpkg-deb --fsys-tarfile linux-image-3.13.0-24-generic-dbgsym_3.13.0-24.47_amd64.ddeb \
   | tar xv --wildcards '**/vmlinux-*' '**/bcache.ko'
./usr/lib/debug/lib/modules/3.13.0-24-generic/kernel/drivers/md/bcache/bcache.ko
./usr/lib/debug/boot/vmlinux-3.13.0-24-generic

$ du -hs usr/lib/debug/
173M    usr/lib/debug/
```

## (eu-)addr2line (address to line)

Two tools, same purpose: to convert an address from stack traces into source code file/line number:
- `eu-addr2line` (from the `elfutils` package), works better with `<function name>+0x<instruction offset>` format.
- `addr2line` (from the `binutils` package), needs absolute addresses for that.

Options:
- `-p`: pretty print for humans (`--pretty-print` on `eu-addr2line`)
- `-i`: show inlined functions
- `-f`: show function names
- `-a`: show addresses
- `-e`: executable (`vmlinux` or `module.ko`)

For example, `SyS_uname+0x1b`:

- `eu-addr2line`:
```
$ eu-addr2line --pretty-print -ifae usr/lib/debug/boot/vmlinux-3.13.0-24-generic SyS_uname+0x1b
0xffffffff8107eb7b: SYSC_uname at /build/buildd/linux-3.13.0/kernel/sys.c:1149
 (inlined by) SyS_uname at /build/buildd/linux-3.13.0/kernel/sys.c:1142
```

- `addr2line`:
```
$ addr2line -pifae usr/lib/debug/boot/vmlinux-3.13.0-24-generic SyS_uname+0x1b # not good; see zero address.
0x0000000000000000: __per_cpu_start at /build/buildd/linux-3.13.0/arch/x86/include/asm/processor.h:415

$ addr2line -pifae usr/lib/debug/boot/vmlinux-3.13.0-24-generic ffffffff8107eb7b # address found with disassembly/math below.
0xffffffff8107eb7b: SYSC_uname at /build/buildd/linux-3.13.0/kernel/sys.c:1149
 (inlined by) SyS_uname at /build/buildd/linux-3.13.0/kernel/sys.c:1142
```

For `addr2line`, from the disassembly output: `ffffffff8107eb60 <SyS_uname>:`, which is the function's base address.
The offset to the base address is `0x1b`, so `ffffffff8107eb60 + 1b = ffffffff8107eb7b` (used above.)



Checking the line numbers:
```
$ git show Ubuntu-3.13.0-24.47:kernel/sys.c | less -N
...
   1142 SYSCALL_DEFINE1(uname, struct old_utsname __user *, name)
   1143 {
   1144         int error = 0;
   1145 
   1146         if (!name)
   1147                 return -EFAULT;
   1148 
   1149         down_read(&uts_sem);
...
```

And the disassembly indeed reveals a function call to `down_read`:  
`ffffffff8107eb7b:       e8 a0 d9 69 00          callq  ffffffff8171c520 <down_read>`

## pahole (p-a-hole, pick a hole)

The `pahole` tool (from the `dwarves` package) is useful to find `struct` member offsets.
Specially to confirm the faulting instruction address in `NULL` pointer dereferences after you know which `struct` it referred to.

```
$ pahole --hex -C dentry usr/lib/debug/boot/vmlinux-3.13.0-24-generic 
die__process_function: tag not supported (INVALID)!
struct dentry {
        unsigned int               d_flags;              /*     0   0x4 */
        seqcount_t                 d_seq;                /*   0x4   0x4 */
        struct hlist_bl_node       d_hash;               /*   0x8  0x10 */
        struct dentry *            d_parent;             /*  0x18   0x8 */
        struct qstr                d_name;               /*  0x20  0x10 */
        struct inode *             d_inode;              /*  0x30   0x8 */
        unsigned char              d_iname[32];          /*  0x38  0x20 */
...
        /* size: 192, cachelines: 3, members: 16 */
};
```

## decodecode (decode code)

Remeber that `Code:` line from the trace? Let's use it here.
```
[ 16.534841] Code: 00 00 0f 1f 44 00 00 55 48 89 e5 41 55 41 54 53 49 89 d5 ba 01 00 00 00 48 89 fb e8 22 ff ff ff 49 89 c4 48 89 c2 eb 03 48 89 ca <48> 8b 4a 08 4c 09 2a 48 85 c9 75 f1 4c 89 62 08 48 8b 43 08 48
```

The `decodecode` script is available in [scripts/decodecode](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/plain/scripts/decodecode), and takes that line in standard input:

If you don't have a kernel tree handy:
```
$ wget https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/plain/scripts/decodecode
$ chmod +x decodecode
```

```
$ echo '[ 16.534841] Code: 00 00 0f 1f 44 00 00 55 48 89 e5 41 55 41 54 53 49 89 d5 ba 01 00 00 00 48 89 fb e8 22 ff ff ff 49 89 c4 48 89 c2 eb 03 48 89 ca <48> 8b 4a 08 4c 09 2a 48 85 c9 75 f1 4c 89 62 08 48 8b 43 08 48' | ./decodecode
```

Output:
```
[ 16.534841] Code: 00 00 0f 1f 44 00 00 55 48 89 e5 41 55 41 54 53 49 89 d5 ba 01 00 00 00 48 89 fb e8 22 ff ff ff 49 89 c4 48 89 c2 eb 03 48 89 ca <48> 8b 4a 08 4c 09 2a 48 85 c9 75 f1 4c 89 62 08 48 8b 43 08 48
All code
========
   0:   00 00                   add    %al,(%rax)
   2:   0f 1f 44 00 00          nopl   0x0(%rax,%rax,1)
   7:   55                      push   %rbp
   8:   48 89 e5                mov    %rsp,%rbp
   b:   41 55                   push   %r13
   d:   41 54                   push   %r12
   f:   53                      push   %rbx
  10:   49 89 d5                mov    %rdx,%r13
  13:   ba 01 00 00 00          mov    $0x1,%edx
  18:   48 89 fb                mov    %rdi,%rbx
  1b:   e8 22 ff ff ff          callq  0xffffffffffffff42
  20:   49 89 c4                mov    %rax,%r12
  23:   48 89 c2                mov    %rax,%rdx
  26:   eb 03                   jmp    0x2b
  28:   48 89 ca                mov    %rcx,%rdx
  2b:*  48 8b 4a 08             mov    0x8(%rdx),%rcx           <-- trapping instruction
  2f:   4c 09 2a                or     %r13,(%rdx)
  32:   48 85 c9                test   %rcx,%rcx
  35:   75 f1                   jne    0x28
  37:   4c 89 62 08             mov    %r12,0x8(%rdx)
  3b:   48 8b 43 08             mov    0x8(%rbx),%rax
  3f:   48                      rex.W

Code starting with the faulting instruction
===========================================
   0:   48 8b 4a 08             mov    0x8(%rdx),%rcx
   4:   4c 09 2a                or     %r13,(%rdx)
   7:   48 85 c9                test   %rcx,%rcx
   a:   75 f1                   jne    0xfffffffffffffffd
   c:   4c 89 62 08             mov    %r12,0x8(%rdx)
  10:   48 8b 43 08             mov    0x8(%rbx),%rax
  14:   48                      rex.W
```

So, we can see that the faulting instruction is `mov 0x8(%rdx),%rcx`.
This instruction moves data from the memory address pointed-to by `RDX` plus `0x8` (i.e., base + offset / pointer to struct + struct member) into the `RCX` register.

And checking it against the registers in that trace, indeed `RDX` is zero, and `CR2` (faulting address is `0x8`)
```
[ 16.460954] BUG: unable to handle kernel NULL pointer dereference at 0000000000000008
...
[ 16.479113] RDX: 0000000000000000 ...
...
[ 16.487084] CR2: 0000000000000008 ...
```

Let's check it with `eu-addr2line` and `pahole`:

```
[ 16.463150] IP: create_empty_buffers+0x29/0xf0
...
[ 16.472651] CPU: 0 PID: 1467 Comm: bcache-register Not tainted 4.15.0-110-generic #111-Ubuntu
```

- Get the debug symbols package, and extract `vmlinux`:
```
$ wget http://ddebs.ubuntu.com/pool/main/l/linux/linux-image-unsigned-4.15.0-110-generic-dbgsym_4.15.0-110.111_amd64.ddeb

$ dpkg-deb --fsys-tarfile linux-image-unsigned-4.15.0-110-generic-dbgsym_4.15.0-110.111_amd64.ddeb \
    | tar xv --wildcards '**/vmlinux-*'
./usr/lib/debug/boot/vmlinux-4.15.0-110-generic
```

- Check the source file/line for that instruction:
```
$ eu-addr2line --pretty-print -ifae usr/lib/debug/boot/vmlinux-4.15.0-110-generic create_empty_buffers+0x29
0xffffffff812bd7d9: create_empty_buffers at /build/linux-9WYFfL/linux-4.15.0/fs/buffer.c:1555

$ git show Ubuntu-4.15.0-110.111:fs/buffer.c | less -N
...
   1545 void create_empty_buffers(struct page *page,
   1546                         unsigned long blocksize, unsigned long b_state)
   1547 {
   1548         struct buffer_head *bh, ...
   ...
   1555                 bh = bh->b_this_page;
   ...
```
- Check the struct member offset (`struct buffer_head.b_this_page`):
```
$ pahole --hex -C buffer_head usr/lib/debug/boot/vmlinux-4.15.0-110-generic
die__process_function: tag not supported (INVALID)!
struct buffer_head {
        long unsigned int          b_state;              /*     0   0x8 */
        struct buffer_head *       b_this_page;          /*   0x8   0x8 */
        struct page *              b_page;               /*  0x10   0x8 */
...
```

And sure enough, it is `0x8`; so, `NULL` + `0x8` = `0x8`, reported as the faulting address.

## objdump (object dump) disassembly

This can provide the entire disassembly of the `vmlinux` image or a kernel `module.ko` file.  
Useful for more complex analysis. 

An example of a function call to `_copy_to_user` in `SyS_uname()`, with parameter passing and return value checking.

```
$ objdump -d usr/lib/debug/boot/vmlinux-3.13.0-24-generic > vmlinux-3.13.0-24-generic.objdump

$ vim vmlinux-3.13.0-24-generic.objdump
...
ffffffff8107eb60 <SyS_uname>:
...
ffffffff8107eb96:       48 89 df                mov    %rbx,%rdi      # RDI = first parameter
ffffffff8107eb99:       48 8b 70 08             mov    0x8(%rax),%rsi # RSI = second parameter
ffffffff8107eb9d:       48 83 c6 04             add    $0x4,%rsi
ffffffff8107eba1:       e8 9a a6 2e 00          callq  ffffffff81369240 <_copy_to_user>
ffffffff8107eba6:       48 85 c0                test   %rax,%rax      # RAX = return value
ffffffff8107eba9:       75 35                   jne    ffffffff8107ebe0 <SyS_uname+0x80>
...
```

# EOF. Thank you! Questions?
