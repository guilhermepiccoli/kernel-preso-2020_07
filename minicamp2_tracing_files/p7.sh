#!/bin/bash
set -v

tracedir="/sys/kernel/debug/tracing"

#Set the nop tracer
echo nop > $tracedir/current_tracer
echo 1 > $tracedir/tracing_on
read

#Add kprobe events
echo 'r:ret_nvme nvme_dev_ioctl ret=$retval:s64' > $tracedir/kprobe_events
#Notice the >>
echo 'p:cmd_nvme nvme_dev_ioctl+0 cmd=%si:x32' >> $tracedir/kprobe_events
read

#Enable both kprobe events
echo 1 > $tracedir/events/kprobes/ret_nvme/enable
echo 1 >> $tracedir/events/kprobes/cmd_nvme/enable

#Equivalent to:
echo 1 > $tracedir/events/kprobes/enable
read

#Run command and right after disable the event tracing
echo > $tracedir/trace
nvme id-ctrl /dev/nvme0 1>/dev/null
read

#Disable and remove the kprobe events - order is important
echo 0 > $tracedir/events/kprobes/enable
echo > $tracedir/kprobe_events
read

#clean-up
echo 0 > $tracedir/tracing_on
echo > $tracedir/trace
