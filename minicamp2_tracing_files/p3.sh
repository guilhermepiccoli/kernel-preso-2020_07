#!/bin/bash
set -v

tracedir="/sys/kernel/debug/tracing"

echo function > $tracedir/current_tracer
echo 1 > $tracedir/tracing_on
read

#clean-up
echo 0 > $tracedir/tracing_on
echo > $tracedir/trace
