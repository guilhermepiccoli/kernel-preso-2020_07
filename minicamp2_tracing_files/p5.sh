#!/bin/bash
set -v

tracedir="/sys/kernel/debug/tracing"

#Enable function graph tracer this time
echo function_graph > $tracedir/current_tracer
read

#Filter to show only one function call trace
echo nvme_dev_ioctl > $tracedir/set_graph_function

#It helps to see the name of the functions on closing brackets
echo 1 > $tracedir/options/funcgraph-tail
echo 1 > $tracedir/tracing_on
read

#Run nvme-cli and check trace
nvme id-ctrl /dev/nvme0 1>/dev/null
read

#"Hack" the execution time, introducing a delay
insmod /root/nvmedelay.ko

echo > $tracedir/trace
nvme id-ctrl /dev/nvme0 1>/dev/null
read

#clean-up
rmmod /root/nvmedelay.ko
echo  > $tracedir/set_graph_function
echo 0 > $tracedir/tracing_on
echo > $tracedir/trace
