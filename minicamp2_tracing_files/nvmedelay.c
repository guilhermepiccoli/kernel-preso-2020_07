/* Simple delay for nvme driver, as a demonstration.
 * Tested on Bionic kernel 4.15.0-111
 *
 * Guilherme G. Piccoli (2020)
 */

#include <linux/kprobes.h>
#include <linux/module.h>
#include <linux/delay.h>


static struct kprobe kp1 = {
	.symbol_name = "nvme_user_cmd",
	.offset = 0x0,
};

static int pre_handler(struct kprobe *p, struct pt_regs *regs)
{
	mdelay(1200);
	return 0;
}

static int __init kprobe_init(void)
{
	int ret;

	kp1.pre_handler = pre_handler;
	ret = register_kprobe(&kp1);
	if (ret < 0) {
		pr_err("kprobe FAILED to activate (%d)\n", ret);
		return ret;
	}
	return 0;
}

static void __exit kprobe_exit(void)
{
	unregister_kprobe(&kp1);
}

module_init(kprobe_init)
module_exit(kprobe_exit)
MODULE_LICENSE("GPL");
