#!/bin/bash
set -v

tracedir="/sys/kernel/debug/tracing"

#Set the nop tracer
echo nop > $tracedir/current_tracer
echo 1 > $tracedir/tracing_on
read

#Enable event traces for the ioctl syscall (enter/exit):
echo 1 > $tracedir/events/syscalls/sys_enter_ioctl/enable
echo 1 > $tracedir/events/syscalls/sys_exit_ioctl/enable
read

#Run command and right after disable the event tracing
echo > $tracedir/trace
taskset -c 1 nvme id-ctrl /dev/nvme0 1>/dev/null
echo 0 > $tracedir/events/enable
read

#clean-up
echo 0 > $tracedir/tracing_on
echo > $tracedir/trace
