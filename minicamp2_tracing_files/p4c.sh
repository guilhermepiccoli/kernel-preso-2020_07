#!/bin/bash
set -v

tracedir="/sys/kernel/debug/tracing"

echo function > $tracedir/current_tracer

#Filter only one function, but also enable stack trace
echo nvme_setup_cmd > $tracedir/set_ftrace_filter
echo 1 > $tracedir/options/func_stack_trace
read

#Start tracing
echo 1 > $tracedir/tracing_on
echo "CMD_STARTED" > $tracedir/trace_marker
nvme id-ctrl /dev/nvme0 1>/dev/null
echo "CMD FINSHED" > $tracedir/trace_marker
read

#Profiling - let's count function executions
echo 0 > $tracedir/options/func_stack_trace
echo > $tracedir/trace 

echo 1 > $tracedir/function_profile_enabled
read

taskset -c 3 nvme id-ctrl /dev/nvme0 1>/dev/null
taskset -c 3 nvme id-ctrl /dev/nvme0 1>/dev/null
taskset -c 3 nvme id-ctrl /dev/nvme0 1>/dev/null
read


#clean-up
echo 0 > $tracedir/function_profile_enabled
echo > $tracedir/set_ftrace_filter
echo 0 > $tracedir/tracing_on
echo > $tracedir/trace

