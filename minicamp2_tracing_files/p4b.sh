#!/bin/bash
set -v

tracedir="/sys/kernel/debug/tracing"

echo function > $tracedir/current_tracer

#Filter only nvme* functions
echo nvme\* > $tracedir/set_ftrace_filter

#Equivalent: filter only _nvme modules_ functions
echo *:mod:nvme* > $tracedir/set_ftrace_filter
read

#Filter-out the nvme_init_queue function
echo \!nvme_init_queue >> $tracedir/set_ftrace_filter
read

#Run nvme-cli and check the trace
echo 1 > $tracedir/tracing_on
nvme id-ctrl /dev/nvme0 1>/dev/null
read

#Trick to capture only a specific PID tracing
echo > $tracedir/trace

mkfifo /root/buf
nvme id-ctrl $(cat /root/buf) &
echo $!
read

#clean-up
echo 0 > $tracedir/tracing_on
echo > $tracedir/trace

echo > $tracedir/set_ftrace_pid
rm -f /root/buf
